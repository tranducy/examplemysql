package example.action;

import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import example.dao.RoomDAO;
import example.dao.StudentDAO;
import example.dao.StudentDAOImpl;
import example.entity.Room;
import example.entity.Student;

@Results(value={
		@Result(name="view", location="/Student.jsp"),
		@Result(name="success", type="redirectAction", params={"actionName", "viewStu"})
})
public class StuentAction extends ActionSupport implements ModelDriven<Student> {
	
	StudentDAOImpl stuDAO = new StudentDAOImpl();

	RoomDAO roomDAO = new RoomDAO();
	
	@Action(value="/viewStu")
	public String viewStu() {
		return "view";
	}
	
	@Action(value="/insertstudent")
	public String insertStu() {
		stuDAO.insertStudent(model);
		return "view";
	}
	
	@Action(value="/deleteStudent")
	public String deleteStu() {
		stuDAO.deleteStudent(model);
		return "view";
	}
	
	@Action(value="/editStudent")
	public String loadStuById() {
		stuDAO.refreshStu(model);
		return "view";
	}
	
	@Action(value="/updateStudent")
	public String updateStu() {
		stuDAO.updateStu(model);
		return "view";
	}
	
	@Action(value="/newStu")
	public String addNew() {
		return "view";
	}
	
	public List<Student> getStudents() {
		return stuDAO.getStudent();
	}
	
	public List<Room> getRooms() {
		return roomDAO.getRoom();
	}
	
	Student model = new Student();
	@Override
	public Student getModel() {
		return model;
	}
	
	public void test () {
		System.out.println("test 1");
	}
}
