package example.action;

import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import example.dao.RoomDAO;
import example.entity.Room;

@Results(value={
		@Result(name="view", location="/Room.jsp")
})
public class RoomAction extends ActionSupport implements ModelDriven<Room> {
	
	RoomDAO roomDAO = new RoomDAO();
	
	@Action(value="/viewRoom")
	public String viewRoom() {
		return "view";
	}
	public List<Room> getRooms() {
		return roomDAO.getRoom();
	}
	
	Room model = new Room();
	@Override
	public Room getModel() {
		return model;
	}
	
	
	
	
}
