package example.dao;

import java.util.ArrayList;
import java.util.List;

import myexample.util.HibernateUtil;

import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;

import example.entity.Student;

public class StudentDAOImpl implements StudentDAO {
	/**
	 * Add new sutdent
	 * @param stu
	 */
	@Override
	public void insertStudent(Student stu) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction trans = session.beginTransaction();
		try {
			session.save(stu);
			trans.commit();
		} catch(Exception ex) {
			trans.rollback();
		}
	}
	
	/**
	 * Load all student
	 * @return stus
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<Student> getStudent() {
		String idRoom = ServletActionContext.getRequest().getParameter("idRoom");
		String hql = "";
		if(idRoom != null) {
			hql = "FROM Student WHERE room.idRoom=" + Integer.parseInt(idRoom);
		} else {
			hql = "FROM Student";
		}
		List<Student> stus = new ArrayList<Student>();
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			Query query = session.createQuery(hql);
			stus = query.list();
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		return stus;
	}
	
	/**
	 * Delete student
	 * @param stu
	 */
	@Override
	public void deleteStudent(Student stu) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction trans = session.beginTransaction();
		try {
			session.delete(stu);
			trans.commit();
		} catch(Exception ex) {
			trans.rollback();
		}
	}

	/**
	 * Load student by id
	 * @param stu
	 */
	@Override
	public void refreshStu(Student stu) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.refresh(stu);
	}
	
	/**
	 * update student
	 * @param stu
	 */
	@Override
	public void updateStu(Student stu) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction trans = session.beginTransaction();
		try {
			session.update(stu);
			trans.commit();
		} catch(Exception ex) {
			trans.rollback();
		}
	}
}
