package example.dao;

import java.util.List;

import example.entity.Student;

public interface StudentDAO {
	/**
	 * add new student
	 * @param stu
	 */
	void insertStudent(Student stu);
	/**
	 * load student list
	 * @return
	 */
	List<Student> getStudent();
	/**
	 * delete student
	 * @param stu
	 */
	void deleteStudent(Student stu);
	/**
	 * load student
	 * @param stu
	 */
	void refreshStu(Student stu);
	/**
	 * update student
	 * @param stu
	 */
	void updateStu(Student stu);
}
