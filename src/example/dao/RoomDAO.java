package example.dao;

import java.util.ArrayList;
import java.util.List;

import myexample.util.HibernateUtil;

import org.hibernate.Query;
import org.hibernate.classic.Session;

import example.entity.Room;

public class RoomDAO {
	@SuppressWarnings("unchecked")
	public List<Room> getRoom() {
		List<Room> rooms = new ArrayList<>();
		Session session = HibernateUtil.getSessionFactory().openSession();
		String hql = "FROM Room";
		Query query = session.createQuery(hql);
		rooms = query.list();
		return rooms;
	}
	
	/*
	public static void main(String[] args) {
		List<Room> rooms = getRoom();
		for(Room room : rooms) {
			System.out.println(">>Name:" + room.getName());
		}
	}
	*/
}