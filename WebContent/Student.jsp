<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>
	<s:form action="inputStudent">
		<s:textfield name="name" label="Name"/>
		<s:textfield name="age" label="Age"/>
		<s:hidden name="idStudent"/>
		
		<s:select name="room.idRoom" label="Room"
								list="rooms" listKey="idRoom" listValue="name"/>			
								
		<s:submit value="Add" action="insertstudent"/>
		<s:submit value="Update" action="updateStudent"/>
	</s:form>
	<br/>
	<table width="500" cellpadding="5" cellspacing="0" border="1">
		<tr>
			<td width="150">Ten</td>
			<td width="150">Tuoi</td>
			<td width="100">Room</td>
			<td width="100">&nbsp;</td>
		</tr>
		<s:iterator value="students">
		<tr>
			<td>${name}</td>
			<td>${age}</td>
			<td>${room.name}</td>
			<td align="center">
				<a class="lnkEdit" href="editStudent?idStudent=${idStudent}">Edit</a>|
				<a class="lnkEdit" href="deleteStudent?idStudent=${idStudent}">Delete</a>
			</td>
		</tr>
		</s:iterator>
	</table>
</body>
</html>